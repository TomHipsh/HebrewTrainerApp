package com.hebrewtrainer.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.support.annotation.NonNull;
import com.hebrewtrainer.general.DataRepository;
import com.hebrewtrainer.db.entities.PictureEntity;
import com.hebrewtrainer.datamodels.Picture;


public class UploadPictureViewModel extends AndroidViewModel {
    private PictureEntity picture;
    private DataRepository repository;

    public UploadPictureViewModel(@NonNull Application application, DataRepository repository) {
        super(application);
        this.repository = repository;
        this.picture = new PictureEntity();
    }

    public Picture getPicture() {
        return this.picture;
    }
    public void setPicture(PictureEntity pic) {
        this.picture = pic;
    }
}
