package com.hebrewtrainer.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.content.Context;
import android.content.SharedPreferences;
import com.hebrewtrainer.general.BasicApp;
import com.hebrewtrainer.db.entities.UserEntity;
import java.util.List;


public class UsersListViewModel extends AndroidViewModel {
    private final MediatorLiveData<List<UserEntity>> observableUsers;

    public UsersListViewModel(Application application) {
        super(application);

        this.observableUsers = new MediatorLiveData<>();
        this.observableUsers.setValue(null);

        LiveData<List<UserEntity>> users = ((BasicApp) application).getRepository().getAllUsers();
        this.observableUsers.addSource(users, this.observableUsers::setValue);
    }

    public LiveData<List<UserEntity>> getUsers() {
        return this.observableUsers;
    }
}
