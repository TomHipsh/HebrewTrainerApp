package com.hebrewtrainer.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import com.hebrewtrainer.general.BasicApp;
import com.hebrewtrainer.db.entities.PictureEntity;
import java.util.List;


public class GameViewModel extends AndroidViewModel {
    private final MediatorLiveData<List<PictureEntity>> observablePictures;
    private Integer index;
    public Boolean isPictureFragmentCreated = false;

    public GameViewModel(Application application) {
        super(application);

        this.observablePictures = new MediatorLiveData<>();
        this.observablePictures.setValue(null);
        LiveData<List<PictureEntity>> allPictures = ((BasicApp)application).getRepository().getAllPictures();
        this.observablePictures.addSource(allPictures, this.observablePictures::setValue);

        this.index = 0;
    }

    public LiveData<List<PictureEntity>> getPictures() {
        return this.observablePictures;
    }

    public Integer getIndex() {
        return this.index;
    }

    public PictureEntity nextPicture() {
        if (this.index < this.getPictures().getValue().size() - 1) {
            this.index++;

            return this.getPictures().getValue().get(this.index);
        }
        else {
            return null;
        }
    }
}