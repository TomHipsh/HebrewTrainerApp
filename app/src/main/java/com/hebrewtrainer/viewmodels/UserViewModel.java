package com.hebrewtrainer.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.databinding.ObservableField;
import android.support.annotation.NonNull;
import com.hebrewtrainer.general.BasicApp;
import com.hebrewtrainer.general.DataRepository;
import com.hebrewtrainer.db.entities.UserEntity;


public class UserViewModel extends AndroidViewModel {
    private final LiveData<UserEntity> observableUser;
    private final String userUid;

    public ObservableField<UserEntity> user = new ObservableField<>();

    public UserViewModel(@NonNull Application application,
                         DataRepository repository,
                         final String userUid) {
        super(application);
        this.userUid = userUid;
        this.observableUser = repository.getUser(userUid);
    }

    public LiveData<UserEntity> getObservableUser() {
        return this.observableUser;
    }

    public void setUser(UserEntity user) {
        this.user.set(user);
    }

    /**
     * A creator is used to inject the user UID into the ViewModel
     * <p>
     * This creator is to showcase how to inject dependencies into ViewModels. It's not
     * actually necessary in this case, as the user UID can be passed in a public method.
     */
    public static class Factory extends ViewModelProvider.NewInstanceFactory {

        @NonNull
        private final Application application;
        private final String userUid;
        private final DataRepository repository;


        public Factory(@NonNull Application application, String userUid) {
            this.application = application;
            this.userUid = userUid;
            this.repository = ((BasicApp) application).getRepository();
        }

        @Override
        public <T extends ViewModel> T create(Class<T> modelClass) {
            //noinspection unchecked
            return (T) new UserViewModel(this.application, this.repository, this.userUid);
        }
    }
}