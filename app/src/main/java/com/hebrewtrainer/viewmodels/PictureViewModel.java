package com.hebrewtrainer.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.databinding.ObservableField;
import android.support.annotation.NonNull;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.hebrewtrainer.general.BasicApp;
import com.hebrewtrainer.general.DataRepository;
import com.hebrewtrainer.db.entities.PictureEntity;


public class PictureViewModel extends AndroidViewModel {
    private final LiveData<PictureEntity> observablePicture;
    private final String pictureUid;
    private DataRepository repository;

    public ObservableField<PictureEntity> picture = new ObservableField<>();

    public PictureViewModel(@NonNull Application application, DataRepository repository, final String pictureUid) {
        super(application);
        this.pictureUid = pictureUid;
        this.repository = repository;
        this.observablePicture = repository.getPicture(pictureUid);
    }

    public LiveData<PictureEntity> getObservablePicture() {
        return this.observablePicture;
    }

    public void setPicture(PictureEntity pic) {
        this.picture.set(pic);
    }

    public void addScoreToUser() {
        FirebaseUser fireUser = FirebaseAuth.getInstance().getCurrentUser();
        repository.addScore(fireUser);
    }


    /**
     * A creator is used to inject the picture UID into the ViewModel
     * <p>
     * This creator is to showcase how to inject dependencies into ViewModels. It's not
     * actually necessary in this case, as the picture UID can be passed in a public method.
     */
    public static class Factory extends ViewModelProvider.NewInstanceFactory {

        @NonNull
        private final Application application;
        private final String pictureUid;
        private final DataRepository repository;

        public Factory(@NonNull Application application, String picUid) {
            this.application = application;
            this.pictureUid = picUid;
            this.repository = ((BasicApp)application).getRepository();
        }

        @Override
        public <T extends ViewModel> T create(Class<T> modelClass) {
            //noinspection unchecked
            return (T) new PictureViewModel(this.application, this.repository, this.pictureUid);
        }
    }
}