package com.hebrewtrainer.ui;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.hebrewtrainer.R;
import com.hebrewtrainer.databinding.GameFragmentBinding;
import com.hebrewtrainer.db.entities.PictureEntity;
import com.hebrewtrainer.datamodels.Picture;
import com.hebrewtrainer.interfaces.PictureSubmitCallback;
import com.hebrewtrainer.viewmodels.GameViewModel;
import java.util.List;


public class GameFragment extends Fragment {
    public static final String TAG = "GameFragment";

    List<? extends Picture> picturesList;
    GameViewModel viewModel;
    private GameFragmentBinding binding;

    public final PictureSubmitCallback pictureSubmitCallback = pic -> {
        if (getLifecycle().getCurrentState().isAtLeast(Lifecycle.State.STARTED)) {
            if (getActivity() != null) {
                ((GameActivity) getActivity()).show(pic);
            }
        }
    };

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        binding = DataBindingUtil.inflate(inflater, R.layout.game_fragment, container, false);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.viewModel =
                ViewModelProviders.of(this).get(GameViewModel.class);
        subscribeUi(viewModel);
    }

    private void subscribeUi(GameViewModel viewModel) {

        // Updates the list when the data changes
        viewModel.getPictures().observe(this, pictures -> {
            if (pictures != null) {
                binding.setIsLoading(false);
                this.picturesList = pictures;
                if (pictures.size() > 0 && !viewModel.isPictureFragmentCreated) {
                    ((GameActivity) getActivity()).show(this.viewModel.getPictures().getValue().get(this.viewModel.getIndex()));
                    viewModel.isPictureFragmentCreated= true;
                }
            } else {
                binding.setIsLoading(true);
            }
            binding.executePendingBindings();
        });
    }

    public void nextPic() {
        PictureEntity picture = this.viewModel.nextPicture();
        if (picture != null) {
            ((GameActivity) getActivity()).show(picture);
        }
        else {
            FragmentManager ft = getFragmentManager();
            FinishedGameFragment finishedGameFragment = new FinishedGameFragment();
            finishedGameFragment.show(ft, FinishedGameFragment.TAG);
        }
    }
}
