package com.hebrewtrainer.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import com.hebrewtrainer.R;
import com.hebrewtrainer.datamodels.Picture;
import com.hebrewtrainer.interfaces.NoticeDialogListener;

public class GameActivity extends AppCompatActivity implements NoticeDialogListener {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        // Adds game fragment if this is the first creation
        if (savedInstanceState == null) {
            GameFragment gameFragment = new GameFragment();

            getSupportFragmentManager().beginTransaction()
                    .add(R.id.picture_fragment_container, gameFragment, GameFragment.TAG).commit();
        }
    }

    @Override
    public void onDialogPositiveClick(android.support.v4.app.DialogFragment dialog) {
        switch (dialog.getTag()) {
            case AddScoreFragment.TAG:
                GameFragment gameFragment = (GameFragment)getSupportFragmentManager().findFragmentByTag(GameFragment.TAG);
                gameFragment.nextPic();
                break;
            case FinishedGameFragment.TAG:
                NavUtils.navigateUpFromSameTask(this);
                break;
        }
    }

    public void show(Picture picture) {
        PictureFragment pictureFragment = PictureFragment.forPicture(picture.getUid());
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.child_fragment_container,
                        pictureFragment, null).commit();
    }
}
