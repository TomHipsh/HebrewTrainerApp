package com.hebrewtrainer.ui;

import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.hebrewtrainer.R;
import com.hebrewtrainer.databinding.UserItemBinding;
import com.hebrewtrainer.datamodels.User;
import com.hebrewtrainer.interfaces.UserClickCallback;
import java.util.List;
import java.util.Objects;



class UsersAdapter extends RecyclerView.Adapter<UsersAdapter.UserViewHolder>{
    List<? extends User> usersList;

    @Nullable
    private final UserClickCallback userClickCallback;

    public UsersAdapter(@Nullable UserClickCallback clickCallback) {
        this.userClickCallback = clickCallback;
    }

    public void setUserList(final List<? extends User> usersList) {
        if (this.usersList == null) {
            this.usersList = usersList;
            notifyItemRangeChanged(0, usersList.size());
        }
        else {
            DiffUtil.DiffResult result = DiffUtil.calculateDiff(new DiffUtil.Callback() {
                @Override
                public int getOldListSize() {
                    return usersList.size();
                }

                @Override
                public int getNewListSize() {
                    return usersList.size();
                }

                @Override
                public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
                    return usersList.get(oldItemPosition).getUid().equals(usersList.get(newItemPosition).getUid());
                }

                @Override
                public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
                    User newUser = usersList.get(newItemPosition);
                    User oldUser = usersList.get(oldItemPosition);

                    return newUser.getUid().equals(oldUser.getUid())
                            && Objects.equals(newUser.getEmail(), oldUser.getEmail())
                            && Objects.equals(newUser.getLastUpdatedTimeStamp(), oldUser.getLastUpdatedTimeStamp())
                            && Objects.equals(newUser.getScore(), oldUser.getScore());
                }
            });

            this.usersList = usersList;
            result.dispatchUpdatesTo(this);
        }
    }

    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        UserItemBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.user_item,
                        parent, false);
        binding.setCallback(this.userClickCallback);
        return new UserViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(UserViewHolder holder, int position) {
        holder.binding.setUser(this.usersList.get(position));
        holder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return this.usersList == null ? 0 : this.usersList.size();
    }

    static class UserViewHolder extends RecyclerView.ViewHolder {
        final UserItemBinding binding;

        public UserViewHolder(UserItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
