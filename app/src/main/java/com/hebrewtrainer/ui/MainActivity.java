package com.hebrewtrainer.ui;

import android.arch.lifecycle.ViewModelProviders;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import com.hebrewtrainer.R;
import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.hebrewtrainer.general.BasicApp;
import com.hebrewtrainer.db.entities.UserEntity;
import com.hebrewtrainer.viewmodels.GameViewModel;
import com.hebrewtrainer.viewmodels.UsersListViewModel;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    /**
     * The google sign in client
     */
    private GoogleSignInClient googleSignInClient;

    /**
     * Firebase auth object
     */
    private FirebaseAuth auth;

    /**
     * Random number for sign in activity
     */
    private static final Integer RC_SIGN_IN = 9999;

    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Configures sign-in to request the user's ID, email address, and basic profile.
        // ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.web_client_auth))
                .requestEmail()
                .build();

        // Builds a GoogleSignInClient with the options specified by gso.
        this.googleSignInClient = GoogleSignIn.getClient(this, gso);

        // Starts up firebase authentication
        this.auth = FirebaseAuth.getInstance();

        // Attaches listener
        SignInButton signInButton = findViewById(R.id.sign_in_button);
        signInButton.setStyle(SignInButton.SIZE_WIDE, SignInButton.COLOR_AUTO);
        this.setSignInText(signInButton, getString(R.string.sign_in));
        signInButton.setOnClickListener(this);

        final Button button = findViewById(R.id.button_sign_out);
        button.setOnClickListener(v -> {
            this.signOut();
        });

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(view -> {
            Intent intent = new Intent(getApplicationContext(), AddPictureActivity.class);
            startActivity(intent);
        });

        // Initializes the RoomDB
        ViewModelProviders.of(this).get(UsersListViewModel.class);
        ViewModelProviders.of(this).get(GameViewModel.class);
    }

    @Override
    protected void onStart() {
        super.onStart();

        // Checks if the user is signed in (non-null) and updates the UI accordingly
        FirebaseUser currentUser = this.auth.getCurrentUser();

        if (currentUser != null) {
            findViewById(R.id.sign_in_button).setVisibility(View.GONE);
            findViewById(R.id.button_sign_out).setVisibility(View.VISIBLE);
        }
        else {
            findViewById(R.id.button_sign_out).setVisibility(View.GONE);
            findViewById(R.id.sign_in_button).setVisibility(View.VISIBLE);
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            }
            else {

                // No explanation needed; request the permission
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE},
                        666);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
        else {
            // Permission has already been granted
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 666: {

                // If the request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }

                return;
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case (R.id.sign_in_button):
                this.signIn();
                break;
            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach a listener
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void signOut() {
        ((BasicApp)getApplication()).getRepository().deleteUser(FirebaseAuth.getInstance().getCurrentUser().getUid());
        FirebaseAuth.getInstance().signOut();
        findViewById(R.id.button_sign_out).setVisibility(View.GONE);
        findViewById(R.id.sign_in_button).setVisibility(View.VISIBLE);
    }

    /**
     * Starts google sign in intent
     */
    private void signIn() {
        Intent signInIntent = this.googleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    /**
     * Handle sign in result from google sign in
     * @param task The task from google sign in object
     */
    private void handleSignInResult(Task<GoogleSignInAccount> task) {
        try {
            GoogleSignInAccount account = task.getResult(ApiException.class);
            Log.i(TAG, "User authenticated with google - sending to firebase");

            // Authenticates the account to firebase server
            this.firebaseAuthWithGoogle(account);
        }
        catch (ApiException e) {

            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
            Toast.makeText(getApplicationContext(),
                    "There is an error signing you in. Please contact developer",
                    Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Authenticates user to firebase with google sign in
     * @param acct Google account after sign up with google
     */
    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        this.auth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {

                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = auth.getCurrentUser();

                            UserEntity userEntity = new UserEntity(user.getUid(), user.getEmail(), 0, System.currentTimeMillis(), 0);
                            ((BasicApp) getApplication()).getRepository().insertUser(userEntity);

                            findViewById(R.id.sign_in_button).setVisibility(View.GONE);
                            findViewById(R.id.button_sign_out).setVisibility(View.VISIBLE);
                        }
                        else {

                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Toast.makeText(getApplicationContext(),
                                       "There is an error signing you in. Please contact developer",
                                           Toast.LENGTH_SHORT).show();
                            findViewById(R.id.button_sign_out).setVisibility(View.GONE);
                            findViewById(R.id.sign_in_button).setVisibility(View.VISIBLE);
                        }
                    }
                });
    }

    /**
     * Opens user lists view
     * @param view Clicked view
     */
    public void openUserLists(View view) {
        Intent intent = new Intent(this, UsersActivity.class);
        startActivity(intent);
    }

    /**
     * Changes the text of the SignInButton
     * @param signInButton The sign in button
     * @param buttonText The text to set
     */
    protected void setSignInText(SignInButton signInButton, String buttonText) {

        // Finds the TextView that is inside of the SignInButton and set its text
        for (int i = 0; i < signInButton.getChildCount(); i++) {
            View v = signInButton.getChildAt(i);

            if (v instanceof TextView) {
                TextView tv = (TextView) v;
                tv.setText(buttonText);
                return;
            }
        }
    }

    /**
     * Starts a game
     * @param view Clicked view
     */
    public void startGame(View view) {
        Intent intent = new Intent(this, GameActivity.class);
        startActivity(intent);
    }
}
