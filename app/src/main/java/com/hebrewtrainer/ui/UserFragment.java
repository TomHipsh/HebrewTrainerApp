package com.hebrewtrainer.ui;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.hebrewtrainer.R;
import com.hebrewtrainer.databinding.UserFragmentBinding;
import com.hebrewtrainer.viewmodels.UserViewModel;


public class UserFragment extends Fragment {
    private static final String KEY_USER_ID = "user_id";
    private UserFragmentBinding binding;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        // Inflates this data binding layout
        binding = DataBindingUtil.inflate(inflater, R.layout.user_fragment, container, false);

        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        UserViewModel.Factory userFactory = new UserViewModel.Factory(
                getActivity().getApplication(), getArguments().getString(KEY_USER_ID));

        final UserViewModel model = ViewModelProviders.of(this, userFactory)
                .get(UserViewModel.class);

        binding.setUserViewModel(model);

        subscribeToModel(model);
    }

    private void subscribeToModel(final UserViewModel model) {
        // Observe product data
        model.getObservableUser().observe(this, model::setUser);
    }

    /** Creates user fragment for a specific user UID */
    public static UserFragment forUser(String userUid) {
        UserFragment fragment = new UserFragment();
        Bundle args = new Bundle();
        args.putString(KEY_USER_ID, userUid);
        fragment.setArguments(args);

        return fragment;
    }
}
