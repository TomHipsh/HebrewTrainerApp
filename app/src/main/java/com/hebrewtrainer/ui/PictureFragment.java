package com.hebrewtrainer.ui;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import com.hebrewtrainer.R;
import com.hebrewtrainer.databinding.PictureFragmentBinding;
import com.hebrewtrainer.viewmodels.PictureViewModel;


public class PictureFragment extends Fragment {
    private static final String KEY_PICTURE_ID = "picture_id";
    public static final String TAG = "PictureFragment";

    private PictureFragmentBinding binding;
    public PictureViewModel model;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        // Inflates this data binding layout
        binding = DataBindingUtil.inflate(inflater, R.layout.picture_fragment, container, false);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        PictureViewModel.Factory pictureFactory = new PictureViewModel.Factory(
                getActivity().getApplication(), getArguments().getString(KEY_PICTURE_ID));

        this.model = ViewModelProviders.of(this, pictureFactory)
                .get(PictureViewModel.class);
        binding.setPictureViewModel(model);

        subscribeToModel(model);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Button clickButton = view.findViewById(R.id.checkTranslationButton);
        clickButton.setOnClickListener(v -> {
            checkTranslation(view);
        });
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    private void subscribeToModel(final PictureViewModel model) {

        // Observes picture data
        model.getObservablePicture().observe(this, pictureEntity -> {
            if (pictureEntity.getBitmap() == null) {
                binding.setIsLoading(true);
            }
            else {
                this.model.setPicture(pictureEntity);
                binding.setIsLoading(false);
            }

            binding.executePendingBindings();
        });
    }

    /** Creates picture fragment for a specific picture UID */
    public static PictureFragment forPicture(String picUid) {
        PictureFragment fragment = new PictureFragment();
        Bundle args = new Bundle();
        args.putString(KEY_PICTURE_ID, picUid);
        fragment.setArguments(args);

        return fragment;
    }

    public void checkTranslation(View v) {
        EditText editText = v.findViewById(R.id.translation);

        if (!model.getObservablePicture().getValue().getHebrewText().equals(
                editText.getText().toString())) {
            editText.setError("oops you made a mistake, try again");
        }
        else {
            model.addScoreToUser();
            editText.setTextColor(Color.GREEN);
            FragmentManager ft = getFragmentManager();
            AddScoreFragment addScoreFragment = new AddScoreFragment();
            addScoreFragment.show(ft, AddScoreFragment.TAG);
        }
    }
}
