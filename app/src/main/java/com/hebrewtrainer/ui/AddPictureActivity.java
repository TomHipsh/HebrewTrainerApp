package com.hebrewtrainer.ui;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresPermission;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.hebrewtrainer.general.BasicApp;
import com.hebrewtrainer.R;
import com.hebrewtrainer.db.entities.PictureEntity;
import com.hebrewtrainer.viewmodels.UploadPictureViewModel;
import java.io.IOException;
import java.util.UUID;


public class AddPictureActivity extends AppCompatActivity {
    private UploadPictureViewModel viewModel;

    private boolean isPictureSelected;
    private boolean isEnglishTextSet;
    private boolean isHebrewTextSet;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_picture);

        Button uploadButton = (Button) findViewById(R.id.uploadButton);
        uploadButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(v.getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                getFileFromGallery();
            }
        });

        Button submitButton = (Button) findViewById(R.id.submitPicture);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitPicture();
            }
        });

        viewModel = new UploadPictureViewModel(this.getApplication(), ((BasicApp)getApplication()).getRepository());

        this.isPictureSelected = false;
        this.isEnglishTextSet = false;
        this.isHebrewTextSet = false;

        ((EditText)findViewById(R.id.hebrewInput)).addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                isHebrewTextSet = (s.length() != 0);
                setSumbitButtonVisibility();
            }
        });

        ((EditText)findViewById(R.id.englishInput)).addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                isEnglishTextSet = (s.length() != 0);
                setSumbitButtonVisibility();
            }
        });

        setSumbitButtonVisibility();
    }

    private void setSumbitButtonVisibility() {
        int visibilityToSet;
        Button submitButton = (Button) findViewById(R.id.submitPicture);

        if (this.isPictureSelected && this.isEnglishTextSet && this.isHebrewTextSet) {
            visibilityToSet = View.VISIBLE;
        }
        else {
            visibilityToSet = View.INVISIBLE;
        }

        if (submitButton.getVisibility() != visibilityToSet) {
            submitButton.setVisibility(visibilityToSet);
        }
    }

    private void submitPicture() {
        PictureEntity pictureEntity = (PictureEntity)this.viewModel.getPicture();
        pictureEntity.setHebrewText(((EditText)findViewById(R.id.hebrewInput)).getText().toString());
        pictureEntity.setEnglishText(((EditText)findViewById(R.id.englishInput)).getText().toString());
        String pictureUUID = UUID.randomUUID().toString();
        pictureEntity.setUid(pictureUUID);
        pictureEntity.setPictureUrl(pictureUUID + ".jpg");

        findViewById(R.id.loading_pic_cloud).setVisibility(View.VISIBLE);

        ((BasicApp)getApplication()).getRepository().addPicture(pictureEntity, (databaseError, databaseReference) -> {
            findViewById(R.id.loading_pic_cloud).setVisibility(View.INVISIBLE);
            Snackbar.make(findViewById(R.id.add_picture_activity), "The Image was uploaded successfully" , Snackbar.LENGTH_LONG)
                    .setAction("close", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                        }
                    })
                    .addCallback(new Snackbar.Callback() {
                        @Override
                        public void onShown(Snackbar sb) {
                            super.onShown(sb);
                        }

                        @Override
                        public void onDismissed(Snackbar transientBottomBar, int event) {
                            finish();
                        }
                    })
                    .setActionTextColor(getResources().getColor(R.color.colorPrimaryDark ))
                    .show();
        });
    }

    @Override
    @RequiresPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Here we need to check if the activity that was triggers was the Image Gallery.
        // If it is the requestCode will match the LOAD_IMAGE_RESULTS value.
        // If the resultCode is RESULT_OK and there is some data we know that an image was picked.
        if (requestCode == 667 && resultCode == RESULT_OK) {
            if (data != null) {
                Uri photoUri = data.getData();

                try {
                    Bitmap selectedImage = MediaStore.Images.Media.getBitmap(this.getContentResolver(), photoUri);

                    // Creates a copy of bitmap image
                    Bitmap bmpCopy = selectedImage.copy(selectedImage.getConfig(), true);

                    PictureEntity pictureEntity = new PictureEntity(this.viewModel.getPicture());
                    pictureEntity.setBitmap(bmpCopy);
                    this.viewModel.setPicture(pictureEntity);
                    this.isPictureSelected = true;
                    setSumbitButtonVisibility();

                    ((ImageView)findViewById(R.id.previewPic)).setImageBitmap(bmpCopy);
                }
                catch (IOException e) {
                    e.printStackTrace();
                    this.isPictureSelected = false;
                    setSumbitButtonVisibility();
                    ((ImageView)findViewById(R.id.previewPic)).setImageDrawable(null);
                }
            }
            else {
                this.isPictureSelected = false;
                setSumbitButtonVisibility();
                ((ImageView)findViewById(R.id.previewPic)).setImageDrawable(null);
            }
        }
    }

    @RequiresPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
    void getFileFromGallery() {

        // Creates intent for picking a photo from the gallery
        Intent intent = new Intent(Intent.ACTION_PICK,
                                   MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        // If you call startActivityForResult() using an intent that no app can handle, your app will crash.
        // So as long as the result is not null, it's safe to use the intent.
        if (intent.resolveActivity(this.getPackageManager()) != null) {

            // Brings up gallery to select a photo
            startActivityForResult(intent, 667);
        }
    }
}