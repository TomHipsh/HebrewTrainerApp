package com.hebrewtrainer.ui;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.hebrewtrainer.R;
import com.hebrewtrainer.databinding.ListFragmentBinding;
import com.hebrewtrainer.interfaces.UserClickCallback;
import com.hebrewtrainer.viewmodels.UsersListViewModel;

/**
 * The users list fragment. holds user adapter
 */
public class UsersListFragment extends android.support.v4.app.Fragment {
    public  static final String TAG = "UsersListFragment";

    private UsersAdapter usersAdapter;
    private ListFragmentBinding binding;

    private final UserClickCallback userClickCallback = user -> {
        if (getLifecycle().getCurrentState().isAtLeast(Lifecycle.State.STARTED)) {
            if (getActivity() != null) {
                ((UsersActivity) getActivity()).show(user);
            }
        }
    };

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.list_fragment, container, false);
        this.usersAdapter = new UsersAdapter(userClickCallback);
        binding.usersList.setAdapter(usersAdapter);

        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final UsersListViewModel viewModel =
                ViewModelProviders.of(this).get(UsersListViewModel.class);

        subscribeUi(viewModel);
    }

    private void subscribeUi(UsersListViewModel viewModel) {

        // Updates the list when the data changes
        viewModel.getUsers().observe(this, users -> {
            if (users != null) {
                binding.setIsLoading(false);
                usersAdapter.setUserList(users);
            } else {
                binding.setIsLoading(true);
            }
            binding.executePendingBindings();
        });
    }
}
