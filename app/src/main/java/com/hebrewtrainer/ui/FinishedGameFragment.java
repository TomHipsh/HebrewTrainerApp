package com.hebrewtrainer.ui;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import com.hebrewtrainer.interfaces.NoticeDialogListener;

public class FinishedGameFragment extends android.support.v4.app.DialogFragment {
    public static final String TAG = "FinishedGameFragment";

    // Uses this instance of the interface to deliver action events
    NoticeDialogListener listener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        // Verifies that the host activity implements the callback interface
        try {
            // Instantiates the NoticeDialogListener so we can send events to the host
            this.listener = (NoticeDialogListener)context;
        }
        catch (ClassCastException e) {

            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(context.toString()
                    + " must implement NoticeDialogListener");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        // Uses the Builder class for convenient dialog construction
        AlertDialog.Builder builder = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            builder = new AlertDialog.Builder(getActivity());
            builder.setMessage("Congratulations! you bit the game, you translated all the pictures")
                    .setPositiveButton("back to the starting page", (dialog, which) -> {
                        listener.onDialogPositiveClick(this);
                    });
        }

        // Creates the AlertDialog object and return it
        return builder.create();
    }
}
