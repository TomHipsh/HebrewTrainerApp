package com.hebrewtrainer.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.hebrewtrainer.R;
import com.hebrewtrainer.datamodels.User;


public class UsersActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users);

        // Adds users list fragment if this is the first creation
        if (savedInstanceState == null) {
            UsersListFragment fragment = new UsersListFragment();

            getSupportFragmentManager().beginTransaction()
                    .add(R.id.users_fragment_container, fragment, UsersListFragment.TAG).commit();
        }
    }

    public void show(User user) {
        UserFragment userFragment = UserFragment.forUser(user.getUid());
        getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack("users")
                .replace(R.id.users_fragment_container,
                        userFragment, null).commit();
    }
}