package com.hebrewtrainer.general;

import android.app.Application;
import android.content.Context;
import com.hebrewtrainer.db.AppRoomDatabase;
import com.hebrewtrainer.db.AppFirebaseDatabase;
import com.hebrewtrainer.db.PictureStorage;

public class BasicApp extends Application {
    private static Context context;
    private AppExecutors appExecutors;

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
        this.appExecutors = new AppExecutors();
        BasicApp.getContext().getSharedPreferences(Consts.APP_SHARED_PREFERENCES, Context.MODE_PRIVATE).edit().clear().apply();
    }

    public static Context getContext() {
        return context;
    }

    public AppRoomDatabase getDatabase() {
        return AppRoomDatabase.getInstance(this, this.appExecutors);
    }

    public AppFirebaseDatabase getFireDatabase() {
        return AppFirebaseDatabase.getInstance();
    }

    public PictureStorage getPicDataStorage() {
        return PictureStorage.getInstance();
    }

    public DataRepository getRepository() {
        return DataRepository.getInstance(getDatabase(), getFireDatabase(), getPicDataStorage(), this.appExecutors);
    }
}