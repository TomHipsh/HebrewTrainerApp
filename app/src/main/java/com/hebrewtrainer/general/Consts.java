package com.hebrewtrainer.general;

public class Consts {
    public static final String APP_SHARED_PREFERENCES = "hebrew_trainer";
    public static final String ROOM_DATABASE_NAME = "hebrew-trainer-db";
    public static final String APP_PICTURE_STORAGE_DIR = "/hebrew-trainer-photos";
    public static final String USERS_LAST_UPDATED_PREFERENCE_KEY = "usersLastUpdatedTimeStamp";
    public static final String PICTURES_LAST_UPDATED_PREFERENCE_KEY = "picturesLastUpdatedTimeStamp";
    public static final long PICTURE_MAX_SIZE = 3 * 1024 * 1024; // 3 megabytes
}
