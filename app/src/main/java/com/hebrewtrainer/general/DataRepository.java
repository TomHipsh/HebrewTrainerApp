package com.hebrewtrainer.general;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.util.Log;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.hebrewtrainer.db.AppRoomDatabase;
import com.hebrewtrainer.db.AppFirebaseDatabase;
import com.hebrewtrainer.db.PictureStorage;
import com.hebrewtrainer.db.entities.PictureEntity;
import com.hebrewtrainer.db.entities.UserEntity;
import com.hebrewtrainer.datamodels.Picture;
import com.hebrewtrainer.datamodels.User;
import com.hebrewtrainer.interfaces.GetImageListener;
import java.util.List;


public class DataRepository {
    private final static String TAG = "DataRepository";
    private static DataRepository instance;

    private final AppExecutors appExecutors;
    private final AppRoomDatabase roomDatabase;
    private final AppFirebaseDatabase firebaseDatabase;
    private final PictureStorage pictureStorage;
    private MediatorLiveData<List<UserEntity>> observableUsers;
    private MediatorLiveData<List<PictureEntity>> observablePictures;

    private DataRepository(final AppRoomDatabase roomDatabase,
                           AppFirebaseDatabase firebaseDatabase,
                           PictureStorage pictureStorage,
                           AppExecutors appExecutors) {
        this.roomDatabase = roomDatabase;
        this.firebaseDatabase = firebaseDatabase;
        this.pictureStorage = pictureStorage;
        this.appExecutors = appExecutors;

        this.observableUsers = new MediatorLiveData<>();
        this.observableUsers.addSource(this.roomDatabase.userDao().loadAllUsers(),
                userEntities -> {
                    if (roomDatabase.getDatabaseCreated().getValue() != null) {
                        observableUsers.postValue(userEntities);
                    }
                }
        );

        this.observablePictures = new MediatorLiveData<>();
        this.observablePictures.addSource(this.roomDatabase.pictureDao().loadAllPictures(),
                pictureEntities -> {
                    if (roomDatabase.getDatabaseCreated().getValue() != null) {
                        observablePictures.postValue(pictureEntities);
                   }
                }
        );
    }

    public static DataRepository getInstance(final AppRoomDatabase roomDatabase,
                                             AppFirebaseDatabase firebaseDatabase,
                                             PictureStorage pictureStorage,
                                             AppExecutors appExecutors) {
        if (instance == null) {
            synchronized (DataRepository.class) {
                if (instance == null) {
                    instance = new DataRepository(roomDatabase, firebaseDatabase, pictureStorage, appExecutors);
                }
            }
        }

        return instance;
    }

    /**
     * Gets the users list and been notified when the list changes
     */
    public LiveData<List<UserEntity>> getAllUsers() {
        this.appExecutors.getNetworkIO().execute(() -> firebaseDatabase.getUserDaoFirebase().getAllUsersAndObserve(users -> {
            appExecutors.getDiskIO().execute(() -> {
                SharedPreferences sharedPreferences = BasicApp.getContext().getSharedPreferences(Consts.APP_SHARED_PREFERENCES, Context.MODE_PRIVATE);
                float usersLastUpdatedTimeStamp = sharedPreferences.getFloat(Consts.USERS_LAST_UPDATED_PREFERENCE_KEY, 0);

                if (users != null && users.size() > 0) {
                    float recentUpdate = usersLastUpdatedTimeStamp;

                    for (User user : users) {
                        roomDatabase.userDao().insertUser(new UserEntity(user));
                        if (user.getLastUpdatedTimeStamp() > recentUpdate) {
                            recentUpdate = user.getLastUpdatedTimeStamp();
                        }
                    }

                    SharedPreferences.Editor editor = BasicApp.getContext().getSharedPreferences(Consts.APP_SHARED_PREFERENCES, Context.MODE_PRIVATE).edit();
                    editor.putFloat(Consts.USERS_LAST_UPDATED_PREFERENCE_KEY, recentUpdate);
                    editor.apply();
                }
            });
        }));

        return this.observableUsers;
    }

    public LiveData<UserEntity> getUser(final String uid) {
        return this.roomDatabase.userDao().loadUser(uid);
    }

    public void insertUser(User user) {
        this.appExecutors.getNetworkIO().execute(() -> firebaseDatabase.getUserDaoFirebase().insertUser(user, (databaseError, databaseReference) -> {}));
    }

    public void deleteUser(String uid) {
        this.appExecutors.getNetworkIO().execute(() -> firebaseDatabase.getUserDaoFirebase().deleteUser(uid, (databaseError, databaseReference) -> {}));
    }

    public void addScore(FirebaseUser user) {
        this.appExecutors.getDiskIO().execute(() -> {
            UserEntity userEntity = this.roomDatabase.userDao().getUser(user.getUid());
            this.appExecutors.getNetworkIO().execute(() -> {
                firebaseDatabase.getUserDaoFirebase().addScore(userEntity);
            });
        });
    }

    public LiveData<List<PictureEntity>> getAllPictures() {
        this.appExecutors.getNetworkIO().execute(() -> firebaseDatabase.getPictureDaoFirebase().getAllPicturesAndObserve(pictures -> {
            appExecutors.getDiskIO().execute(() -> {
                SharedPreferences sharedPreferences = BasicApp.getContext().getSharedPreferences(Consts.APP_SHARED_PREFERENCES, Context.MODE_PRIVATE);
                float picturesLastUpdatedTimeStamp = sharedPreferences.getFloat(Consts.PICTURES_LAST_UPDATED_PREFERENCE_KEY, 0);

                if (pictures != null && pictures.size() > 0) {
                    float recentUpdate = picturesLastUpdatedTimeStamp;

                    for (Picture pic: pictures) {
                        PictureEntity picEntity = new PictureEntity(pic);
                        roomDatabase.pictureDao().insertPicture(picEntity);

                        if (pic.getLastUpdatedTimeStamp() > recentUpdate) {
                            recentUpdate = pic.getLastUpdatedTimeStamp();
                        }
                    }

                    SharedPreferences.Editor editor = BasicApp.getContext().getSharedPreferences(Consts.APP_SHARED_PREFERENCES, Context.MODE_PRIVATE).edit();
                    editor.putFloat(Consts.PICTURES_LAST_UPDATED_PREFERENCE_KEY, recentUpdate);
                    editor.apply();
                }
            });
        }));

        return this.observablePictures;
    }

    public LiveData<PictureEntity> getPicture(final String uid) {
        LiveData<PictureEntity> pic = this.roomDatabase.pictureDao().loadPicture(uid);
        MediatorLiveData<PictureEntity> picEntityMediatorLiveData = new MediatorLiveData<>();
        picEntityMediatorLiveData.addSource(pic, pictureEntity -> {
            getImage(pictureEntity.getPictureUrl(), new GetImageListener() {
                @Override
                public void onSuccess(Bitmap image) {
                    pictureEntity.setBitmap(image);
                    picEntityMediatorLiveData.postValue(pictureEntity);
                }

                @Override
                public void onFail() {
                    Log.d(TAG, "onFail: failed getting picture");
                }
            });
        });

        return picEntityMediatorLiveData;
    }

    public void addPicture(PictureEntity pictureEntity, DatabaseReference.CompletionListener listener) {
        this.appExecutors.getNetworkIO().execute(() -> {
            firebaseDatabase.getPictureDaoFirebase().insertPicture(pictureEntity, (databaseError, databaseReference) -> {
                Log.d(TAG, "onComplete: saved pic to firebase storage");
                listener.onComplete(databaseError, databaseReference);
            });
        });
    }

    public void getImage(final String url, final GetImageListener listener) {
        this.appExecutors.getDiskIO().execute(() -> {

            // Checks if the image already exist locally
            String fileName = url;
            Bitmap image = pictureStorage.loadImageFromFile(fileName);

            if (image != null) {
                Log.d(TAG,"getImage from local succeeded " + fileName);
                listener.onSuccess(image);
            }
            else {
                appExecutors.getNetworkIO().execute(() -> firebaseDatabase.getPictureDaoFirebase().getImage(url, new GetImageListener() {
                    @Override
                    public void onSuccess(Bitmap image) {
                        String fileName = url;
                        Log.d(TAG,"getImage from firebase succeeded " + fileName);
                        pictureStorage.saveImageToFile(image, fileName);
                        listener.onSuccess(image);
                    }

                    @Override
                    public void onFail() {
                        Log.d(TAG,"getImage from firebase failed ");
                        listener.onFail();
                    }
                }));
            }
        });
    }
}