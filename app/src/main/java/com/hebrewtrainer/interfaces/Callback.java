package com.hebrewtrainer.interfaces;

public interface Callback<T> {
    void onComplete(T data);
}
