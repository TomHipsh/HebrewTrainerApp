package com.hebrewtrainer.interfaces;

public interface NoticeDialogListener {
    void onDialogPositiveClick(android.support.v4.app.DialogFragment dialog);
}
