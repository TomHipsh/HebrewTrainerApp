package com.hebrewtrainer.interfaces;

import com.hebrewtrainer.datamodels.User;

public interface UserClickCallback {
    void onClick(User user);
}
