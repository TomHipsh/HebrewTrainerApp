package com.hebrewtrainer.interfaces;

import com.hebrewtrainer.datamodels.Picture;


public interface PictureSubmitCallback {
    void onClick(Picture pic);
}
