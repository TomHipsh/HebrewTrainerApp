package com.hebrewtrainer.interfaces;

import android.graphics.Bitmap;


public interface GetImageListener{
    void onSuccess(Bitmap image);
    void onFail();
}
