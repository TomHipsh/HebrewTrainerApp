package com.hebrewtrainer.datamodels;

public interface User {
    String getUid();
    String getEmail();
    int getScore();
    float getLastUpdatedTimeStamp();
    int getIsAdmin();
}
