package com.hebrewtrainer.datamodels;

public interface Picture {
    String getUid();
    String getPictureUrl();
    String getHebrewText();
    String getEnglishText();
    float getLastUpdatedTimeStamp();
}
