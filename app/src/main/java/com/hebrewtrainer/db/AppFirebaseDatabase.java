package com.hebrewtrainer.db;

import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.hebrewtrainer.db.dao.firebaseDao.PictureDaoFirebase;
import com.hebrewtrainer.db.dao.firebaseDao.UserDaoFirebase;


public class AppFirebaseDatabase {
    private static AppFirebaseDatabase instance;

    private FirebaseDatabase firebaseDatabase;
    private FirebaseStorage firebaseStorage;
    private final UserDaoFirebase userDaoFirebase;
    private final PictureDaoFirebase pictureDaoFirebase;

    private AppFirebaseDatabase() {
        this.firebaseDatabase = FirebaseDatabase.getInstance();
        this.firebaseStorage = FirebaseStorage.getInstance();
        this.userDaoFirebase = UserDaoFirebase.getInstance(firebaseDatabase);
        this.pictureDaoFirebase = PictureDaoFirebase.getInstance(firebaseDatabase, firebaseStorage);
    }

    public static AppFirebaseDatabase  getInstance() {
        if (instance == null) {
            synchronized (AppFirebaseDatabase .class) {
                if (instance == null) {
                    instance = new AppFirebaseDatabase ();
                }
            }
        }

        return instance;
    }

    public UserDaoFirebase getUserDaoFirebase() {
        return this.userDaoFirebase;
    }

    public PictureDaoFirebase getPictureDaoFirebase() {
        return this.pictureDaoFirebase;
    }
}