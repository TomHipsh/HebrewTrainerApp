package com.hebrewtrainer.db.entities;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;
import com.hebrewtrainer.datamodels.User;

@Entity(tableName = "users")
public class UserEntity implements User{
    @PrimaryKey
    @NonNull
    private String uid;
    private String email;
    private int score;
    private float lastUpdatedTimeStamp;
    private int isAdmin;

    public UserEntity() {
        this.setUid("");
        this.setEmail("");
        this.setScore(0);
        this.setLastUpdatedTimeStamp(System.currentTimeMillis());
        this.setIsAdmin(0);
    }

    @Ignore
    public UserEntity(String uid, String email, int score, float lastUpdatedTimeStamp, int isAdmin) {
        this.uid = uid;
        this.email = email;
        this.score = score;
        this.lastUpdatedTimeStamp = lastUpdatedTimeStamp;
        this.isAdmin = isAdmin;
    }

    @Ignore
    public UserEntity(User user) {
        this.uid = user.getUid();
        this.email = user.getEmail();
        this.score = user.getScore();
        this.lastUpdatedTimeStamp = user.getLastUpdatedTimeStamp();
        this.isAdmin = user.getIsAdmin();
    }

    @Override
    public String getUid() {
        return this.uid;
    }

    public void setUid(@NonNull String uid) {
        this.uid = uid;
    }

    @Override
    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public int getScore() {
        return this.score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    @Override
    public float getLastUpdatedTimeStamp() {
        return this.lastUpdatedTimeStamp;
    }

    public void setLastUpdatedTimeStamp(float lastUpdatedTimeStamp) {
        this.lastUpdatedTimeStamp = lastUpdatedTimeStamp;
    }

    @Override
    public int getIsAdmin() {
        return this.isAdmin;
    }

    public void setIsAdmin(int isAdmin) {
        this.isAdmin = isAdmin;
    }
}
