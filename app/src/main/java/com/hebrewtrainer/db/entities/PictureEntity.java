package com.hebrewtrainer.db.entities;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import com.hebrewtrainer.datamodels.Picture;

@Entity(tableName = "pictures")
public class PictureEntity implements Picture {
    @PrimaryKey
    @NonNull
    private String uid;
    private String pictureUrl;
    private String hebrewText;
    private String englishText;
    private float lastUpdatedTimeStamp;

    @Ignore
    private Bitmap bitmap;

    public PictureEntity() {
        this.setUid("");
        this.setPictureUrl("");
        this.setHebrewText("");
        this.setEnglishText("");
        this.setLastUpdatedTimeStamp(System.currentTimeMillis());
        this.setBitmap(null);
    }

    @Ignore
    public PictureEntity(Picture pic) {
        this.uid = pic.getUid();
        this.pictureUrl = pic.getPictureUrl();
        this.hebrewText = pic.getHebrewText();
        this.englishText = pic.getEnglishText();
        this.lastUpdatedTimeStamp = pic.getLastUpdatedTimeStamp();
        this.bitmap = null;
    }

    @Ignore
    public PictureEntity(@NonNull String uid, String pictureUrl, String hebrewText, String englishText, float lastUpdatedTimeStamp) {
        this.uid = uid;
        this.pictureUrl = pictureUrl;
        this.hebrewText = hebrewText;
        this.englishText = englishText;
        this.lastUpdatedTimeStamp = lastUpdatedTimeStamp;
        this.bitmap = null;
    }

    @NonNull
    @Override
    public String getUid() {
        return this.uid;
    }

    public void setUid(@NonNull String uid) {
        this.uid = uid;
    }

    @Override
    public String getPictureUrl() {
        return this.pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    @Override
    public String getHebrewText() {
        return this.hebrewText;
    }

    public void setHebrewText(String hebrewText) {
        this.hebrewText = hebrewText;
    }

    @Override
    public String getEnglishText() {
        return this.englishText;
    }

    public void setEnglishText(String englishText) {
        this.englishText = englishText;
    }

    @Override
    public float getLastUpdatedTimeStamp() {
        return this.lastUpdatedTimeStamp;
    }

    public void setLastUpdatedTimeStamp(float lastUpdatedTimeStamp) {
        this.lastUpdatedTimeStamp = lastUpdatedTimeStamp;
    }

    @Ignore
    public Bitmap getBitmap() {
        return this.bitmap;
    }

    @Ignore
    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }
}
