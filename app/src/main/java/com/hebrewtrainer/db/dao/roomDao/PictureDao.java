package com.hebrewtrainer.db.dao.roomDao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import com.hebrewtrainer.db.entities.PictureEntity;
import java.util.List;

@Dao
public interface PictureDao {
    @Query("SELECT * FROM pictures where uid =  :uid")
    LiveData<PictureEntity> loadPicture(String uid);

    @Query("SELECT * FROM pictures")
    LiveData<List<PictureEntity>> loadAllPictures();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertPicture(PictureEntity... pic);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAllPictures(List<PictureEntity> pics);
}
