package com.hebrewtrainer.db.dao.roomDao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import com.hebrewtrainer.db.entities.UserEntity;
import java.util.List;

@Dao
public interface UserDao {
    @Query("SELECT * FROM users where uid =  :uid")
    LiveData<UserEntity> loadUser(String uid);

    @Query("SELECT * FROM users")
    LiveData<List<UserEntity>> loadAllUsers();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertUser(UserEntity... user);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAllUsers(List<UserEntity> users);

    @Delete
    void deleteUser(UserEntity... user);

    @Query("SELECT * FROM users where uid =  :uid")
    UserEntity getUser(String uid);
}
