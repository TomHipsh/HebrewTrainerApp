package com.hebrewtrainer.db.dao.firebaseDao;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.hebrewtrainer.general.BasicApp;
import com.hebrewtrainer.interfaces.GetImageListener;
import com.hebrewtrainer.general.Consts;
import com.hebrewtrainer.interfaces.Callback;
import com.hebrewtrainer.db.entities.PictureEntity;
import com.hebrewtrainer.datamodels.Picture;
import java.io.ByteArrayOutputStream;
import java.util.LinkedList;
import java.util.List;


public class PictureDaoFirebase {
    private static PictureDaoFirebase instance;

    private final FirebaseDatabase firebaseDatabase;
    private final FirebaseStorage firebaseStorage;

    private PictureDaoFirebase(FirebaseDatabase firebaseDatabase, FirebaseStorage firebaseStorage) {
        this.firebaseDatabase = firebaseDatabase;
        this.firebaseStorage = firebaseStorage;
    }

    public static PictureDaoFirebase getInstance(FirebaseDatabase firebaseDatabase, FirebaseStorage firebaseStorage) {
        if (instance == null) {
            synchronized (PictureDaoFirebase.class) {
                if (instance == null) {
                    instance = new PictureDaoFirebase(firebaseDatabase, firebaseStorage);
                }
            }
        }

        return instance;
    }

    public void getAllPicturesAndObserve(final Callback<List<Picture>> callback) {
        SharedPreferences sharedPreferences = BasicApp.getContext().getSharedPreferences(Consts.APP_SHARED_PREFERENCES, Context.MODE_PRIVATE);
        float picturesLastUpdatedTimeStamp = sharedPreferences.getFloat(Consts.PICTURES_LAST_UPDATED_PREFERENCE_KEY, 0);
        DatabaseReference myRef = this.firebaseDatabase.getReference("pictures");
        Query query = myRef.orderByChild("lastUpdatedTimeStamp").startAt(picturesLastUpdatedTimeStamp);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<Picture> picturesList = new LinkedList<>();
                for (DataSnapshot snap : dataSnapshot.getChildren()) {
                    PictureEntity pic = snap.getValue(PictureEntity.class);
                    picturesList.add(pic);
                }
                callback.onComplete(picturesList);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                callback.onComplete(null);
            }
        });
    }

    public void insertPicture(final PictureEntity pic, DatabaseReference.CompletionListener completionListener) {
        StorageReference httpsReference = this.firebaseStorage.getReference(pic.getUid());
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        pic.getBitmap().compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] data = baos.toByteArray();

        UploadTask uploadTask = httpsReference.putBytes(data);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                Log.d("insertPicture", "onFailure: failed...");
            }
        }).addOnSuccessListener(taskSnapshot -> {
            Log.d("insertPicture", "onSuccess: succeeded!");
        });

        PictureEntity pictureToSave = new PictureEntity(pic);
        pictureToSave.setBitmap(null);
        DatabaseReference myRef = this.firebaseDatabase.getReference("pictures");
        myRef.child(pictureToSave.getUid()).setValue(pictureToSave, completionListener);
    }

    public void getImage(String url, GetImageListener getImageListener) {
        StorageReference httpsReference = this.firebaseStorage.getReference(url.substring(0, url.lastIndexOf(".")));
        httpsReference.getBytes(Consts.PICTURE_MAX_SIZE).addOnSuccessListener(bytes -> {
            Bitmap image = BitmapFactory.decodeByteArray(bytes,0,bytes.length);
            getImageListener.onSuccess(image);
        }).addOnFailureListener(exception -> {
            Log.d("TAG",exception.getMessage());
            getImageListener.onFail();
        });
    }
}