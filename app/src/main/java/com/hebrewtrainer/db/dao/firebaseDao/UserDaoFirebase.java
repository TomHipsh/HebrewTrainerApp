package com.hebrewtrainer.db.dao.firebaseDao;

import android.content.Context;
import android.content.SharedPreferences;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.hebrewtrainer.general.BasicApp;
import com.hebrewtrainer.general.Consts;
import com.hebrewtrainer.interfaces.Callback;
import com.hebrewtrainer.db.entities.UserEntity;
import com.hebrewtrainer.datamodels.User;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class UserDaoFirebase {
    private static UserDaoFirebase instance;

    private final FirebaseDatabase firebaseDatabase;

    private UserDaoFirebase(FirebaseDatabase firebaseDatabase) {
        this.firebaseDatabase = firebaseDatabase;
    }

    public static UserDaoFirebase getInstance(FirebaseDatabase firebaseDatabase) {
        if (instance == null) {
            synchronized (UserDaoFirebase.class) {
                if (instance == null) {
                    instance = new UserDaoFirebase(firebaseDatabase);
                }
            }
        }
        return instance;
    }

    public void getAllUsersAndObserve(final Callback<List<User>> callback) {
        SharedPreferences sharedPreferences = BasicApp.getContext().getSharedPreferences(Consts.APP_SHARED_PREFERENCES, Context.MODE_PRIVATE);
        float usersLastUpdatedTimeStamp = sharedPreferences.getFloat(Consts.USERS_LAST_UPDATED_PREFERENCE_KEY, 0);
        DatabaseReference myRef = this.firebaseDatabase.getReference("users");
        Query query = myRef.orderByChild("lastUpdatedTimeStamp").startAt(usersLastUpdatedTimeStamp);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<User> usersList = new LinkedList<>();
                for (DataSnapshot snap : dataSnapshot.getChildren()) {
                    UserEntity user = snap.getValue(UserEntity.class);
                    usersList.add(user);
                }
                callback.onComplete(usersList);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                callback.onComplete(null);
            }
        });
    }

    public void insertUser(final User user, DatabaseReference.CompletionListener completionListener) {
        DatabaseReference myRef = this.firebaseDatabase.getReference("users");
        myRef.child(user.getUid()).setValue(user, completionListener);
    }

    public void deleteUser(final String uid, DatabaseReference.CompletionListener completionListener) {
        DatabaseReference myRef = this.firebaseDatabase.getReference("users").child(uid);
        myRef.removeValue();
    }

    public void addScore(UserEntity userEntity) {
        DatabaseReference myRef = this.firebaseDatabase.getReference("users").child(userEntity.getUid());
        Map<String, Object> userUpdates = new HashMap<>();
        userUpdates.put("lastUpdatedTimeStamp", System.currentTimeMillis());
        userUpdates.put("score", userEntity.getScore() + 1);
        myRef.updateChildren(userUpdates);
    }
}