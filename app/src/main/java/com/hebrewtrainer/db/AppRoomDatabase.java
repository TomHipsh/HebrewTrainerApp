package com.hebrewtrainer.db;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.arch.persistence.room.migration.Migration;
import android.content.Context;
import android.support.annotation.NonNull;
import com.hebrewtrainer.general.AppExecutors;
import com.hebrewtrainer.db.converters.DateConverter;
import com.hebrewtrainer.db.dao.roomDao.UserDao;
import com.hebrewtrainer.db.dao.roomDao.PictureDao;
import com.hebrewtrainer.db.entities.UserEntity;
import com.hebrewtrainer.db.entities.PictureEntity;
import com.hebrewtrainer.general.Consts;

@Database(entities = {UserEntity.class, PictureEntity.class}, version = 6)
@TypeConverters(DateConverter.class)
public abstract class AppRoomDatabase extends RoomDatabase {
    private static AppRoomDatabase instance;

    private final MutableLiveData<Boolean> isDatabaseCreated = new MutableLiveData<>();
    public abstract UserDao userDao();
    public abstract PictureDao pictureDao();

    public static AppRoomDatabase getInstance(final Context context, final AppExecutors appExecutors) {
        if (instance == null) {
            synchronized (AppRoomDatabase.class) {
                if (instance == null) {
                    instance = buildDatabase(context.getApplicationContext(), appExecutors);
                    instance.updateDatabaseCreated(context.getApplicationContext());
                }
            }
        }
        return instance;
    }

    /**
     * Builds the room database. {@link Builder#build()} only sets up the database configuration and
     * creates a new instance of the database.
     * The SQLite database is only created when it's accessed for the first time.
     */
    private static AppRoomDatabase buildDatabase(final Context appContext,
                                                 final AppExecutors executors) {
        return Room.databaseBuilder(appContext, AppRoomDatabase.class, Consts.ROOM_DATABASE_NAME)
                .addMigrations(new Migration(1, 2) {
                    @Override
                    public void migrate(@NonNull SupportSQLiteDatabase database) {
                        database.execSQL("CREATE TABLE pictures(" +
                                         "uid TEXT PRIMARY KEY NOT NULL," +
                                         "pictureUrl TEXT," +
                                         "hebrewText TEXT," +
                                         "englishText TEXT," +
                                         "lastUpdatedTimeStamp REAL NOT NULL" +
                                         ")");
                    }
                }, new Migration(2, 3) {
                    @Override
                    public void migrate(@NonNull SupportSQLiteDatabase database) {
                        database.execSQL("DROP TABLE users");
                        database.execSQL("CREATE TABLE users(" +
                                         "uid TEXT PRIMARY KEY NOT NULL," +
                                         "email TEXT," +
                                         "score INTEGER NOT NULL," +
                                         "lastUpdatedTimeStamp REAL NOT NULL" +
                                         "isAdmin INTEGER NOT NULL," +
                                         ")");
                    }
                })
                .addCallback(new Callback() {
                    @Override
                    public void onCreate(@NonNull SupportSQLiteDatabase db) {
                        super.onCreate(db);
                        executors.getDiskIO().execute(() -> {

                            // Generates data for pre-population
                            AppRoomDatabase database = AppRoomDatabase.getInstance(appContext, executors);

                            // Notifies that the database has been created and it is ready to be used
                            database.setDatabaseCreated();
                        });
                    }
                }).build();
    }

    /**
     * Checks whether the database already exist and exposes it via {@link #getDatabaseCreated()}
     */
    private void updateDatabaseCreated(final Context context) {
        if (context.getDatabasePath(Consts.ROOM_DATABASE_NAME).exists()) {
            setDatabaseCreated();
        }
    }

    public LiveData<Boolean> getDatabaseCreated() {
        return this.isDatabaseCreated;
    }

    private void setDatabaseCreated() {
        this.isDatabaseCreated.postValue(true);
    }

}
