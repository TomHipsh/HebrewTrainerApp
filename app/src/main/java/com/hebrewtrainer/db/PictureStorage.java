package com.hebrewtrainer.db;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaScannerConnection;
import android.os.Environment;
import android.util.Log;
import com.hebrewtrainer.general.Consts;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import com.hebrewtrainer.general.BasicApp;

public class PictureStorage {
    private final static String TAG = "PictureStorage";
    private static PictureStorage instance;

    private PictureStorage() {}

    public static PictureStorage getInstance() {
        if (instance == null) {
            synchronized (PictureStorage.class) {
                if (instance == null) {
                    instance = new PictureStorage();
                }
            }
        }
        return instance;
    }

    public Bitmap loadImageFromFile(String imageFileName) {
        Bitmap bitmap = null;

        try {
            File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES + Consts.APP_PICTURE_STORAGE_DIR);
            File imageFile = new File(dir,imageFileName);
            InputStream inputStream = new FileInputStream(imageFile);
            bitmap = BitmapFactory.decodeStream(inputStream);
            Log.d(TAG,"Loaded image from cache: " + imageFileName);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return bitmap;
    }

    public void saveImageToFile(Bitmap imageBitmap, String imageFileName) {
        String root = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString();
        File myDir = new File(root + Consts.APP_PICTURE_STORAGE_DIR);
        myDir.mkdirs();
        File file = new File(myDir, imageFileName);

        if (file.exists()) {
            file.delete();
        }

        try {
            FileOutputStream out = new FileOutputStream(file);
            imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        // Tells the media scanner about the new file in order to immediately make it available to the user.
        MediaScannerConnection.scanFile(BasicApp.getContext(), new String[] { file.toString() }, null,
                (path, uri) -> {
                    Log.i("ExternalStorage", "Scanned " + path + ":");
                    Log.i("ExternalStorage", "-> uri=" + uri);
                }
        );
    }
}
